<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    protected Student $student;

    /**
     * @param $student
     */
    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    public function index(){
        $students = $this->student->all();
        return view('students.index',compact('students'));
    }

    public function store(StudentRequest $request){
        $this->student->create($request->all());
        return redirect()->route('students.index');
    }

    public function create(){
        return view('students.create');
    }

    public function update(UpdateStudentRequest $request,$id){
        $student = $this->student->findOrFail($id);
        $dataUpdate = $request->all();
        $student->update($dataUpdate);
        return redirect()->route('students.index');
    }

    public function edit($id){
      $student =  $this->student->findOrFail($id);
        return view('students.edit',compact('student'));
    }

    public function destroy($id){
        $student = $this->student->findOrFail($id);
        $student->delete();
        return redirect()->route('students.index');
    }

    public function show($id){
        $student = $this->student->findOrFail($id);
        return view('students.show',compact('student'));
    }
}
