
@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Update Task</h2>
        <div class="row justify-content-center">
            <form action="{{route('students.update',$student->id)}}" method="post">
                @csrf
                @method('put')
                <div class="form-group">
                    <label for="formGroupExampleInput">Name</label>
                    <input type="text" class="form-control" value="{{$student->name}}" name="name" id="formGroupExampleInput" placeholder="Name">
                    @error('name')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Age</label>
                    <input type="text" class="form-control" value="{{$student->age}}" name="age" id="formGroupExampleInput2" placeholder="Content">
                    @error('age')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput2">Address</label>
                    <input type="text" class="form-control" value="{{$student->address}}" name="address" id="formGroupExampleInput2" placeholder="Content">
                </div>
                <button type="submit" class="btn btn-info">Update</button>
            </form>

        </div>
    </div>
@endsection
