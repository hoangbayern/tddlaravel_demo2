<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/students',[StudentController::class, 'index'])
    ->name('students.index');
Route::post('/students',[StudentController::class, 'store'])
    ->name('students.store')
    ->middleware('auth');
Route::get('/students/create',[StudentController::class, 'create'])
    ->name('students.create')
    ->middleware('auth');
Route::put('/students/{id}',[StudentController::class, 'update'])
    ->name('students.update')
    ->middleware('auth');
Route::get('/students/update/{id}',[StudentController::class, 'edit'])
    ->name('students.edit')
    ->middleware('auth');
Route::delete('/{id}',[StudentController::class, 'destroy'])
    ->name('students.destroy')
    ->middleware('auth');
Route::get('/students/{id}',[StudentController::class, 'show'])
    ->name('students.show')
    ->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
