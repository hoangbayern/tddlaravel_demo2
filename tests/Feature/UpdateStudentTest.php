<?php

namespace Tests\Feature;

use App\Models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateStudentTest extends TestCase
{
    use WithFaker;
    /** @test  */
    public function authenticated_user_can_update_student(){
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->create();
        $dataUp = [
            'name' => $this->faker->name,
            'age' => $this->faker->randomNumber(2),
            'address' => $this->faker->address
        ];
        $response = $this->put($this->UpdateRouteTest($student->id),$dataUp);
        $response->assertStatus(302);
        $dataAfter = Student::find($student->id);
        $dataUpAfter = [
          'name' => $dataAfter->name,
          'age' => $dataAfter->age,
          'address' => $dataAfter->address
        ];
        $this->assertEquals($dataUp,$dataUpAfter,message: 'no update');
        $response->assertRedirect(route('students.index'));
    }

    /** @test  */
    public function authenticated_user_can_view_form_update(){
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->create();
        $response = $this->get($this->UpdateViewRouteTest($student->id));
        $response->assertViewIs('students.edit');
    }

    /** @test  */
    public function unauthenticated_user_can_not_update_student(){
        $student = Student::factory()->create();
        $response = $this->get($this->UpdateViewRouteTest($student->id));
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_not_update_if_name_field_is_null(){
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->create();
        $dataUpdate = [
          'name' => null,
          'age' => $student->age,
          'address' => $student->address
        ];
        $response = $this->put($this->UpdateRouteTest($student->id),$dataUpdate);
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function authenticated_user_can_see_name_required_text_if_validate_error_update()
    {
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->create();
        $dataUpdate = [
            'name' => null,
            'age' => $student->age,
            'address' => $student->address
        ];
        $response = $this->from($this->UpdateViewRouteTest($student->id))->put($this->UpdateRouteTest($student->id),$dataUpdate);
        $response->assertRedirect($this->UpdateViewRouteTest($student->id));
    }

    public function UpdateRouteTest(string $id){
        return route('students.update',$id);
    }
    public function UpdateViewRouteTest(string $id){
        return route('students.edit',$id);
    }
}
