<?php

namespace Tests\Feature;

use App\Models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowStudentTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_show_student()
    {
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->create();
        $response = $this->get(route('students.show', $student->id));
        $response->assertStatus(200);
        $response->assertViewIs('students.show');
        $response->assertSee($student->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_show_student()
    {
        $student = Student::factory()->create();
        $response = $this->get(route('students.show', $student->id));
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticated_user_can_not_show_student_if_not_exist()
    {
        $studentID = -1;
        $response = $this->get(route('students.show', $studentID));
        $response->assertStatus(302);
}
}
