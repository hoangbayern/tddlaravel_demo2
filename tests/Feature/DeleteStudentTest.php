<?php

namespace Tests\Feature;

use App\Models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteStudentTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_delete_student(){
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->create();
        $studentBeforeDelete = Student::count();
        $response = $this->delete($this->DeleteRouteTest($student->id));
        $response->assertStatus(302);
        $response->assertRedirect(route('students.index'));
        $studentAfterDelete = Student::count();
        $this->assertEquals($studentAfterDelete,$studentBeforeDelete-1);
    }

    /** @test  */
    public function unauthenticated_user_can_not_delete_student(){
        $student = Student::factory()->create();
        $response = $this->delete($this->DeleteRouteTest($student->id));
        $response->assertRedirect('/login');
    }

    public function DeleteRouteTest(string $id){
        return route('students.destroy',$id);
    }
}
