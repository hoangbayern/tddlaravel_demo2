<?php

namespace Tests\Feature;

use App\Models\Student;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateStudentTest extends TestCase
{
    /** @test  */
    public function authenticated_user_can_creat_student(){
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->make()->toArray();
        $studentBeforeCreate = Student::count();
        $response =  $this->post($this->CreateRouteTest(),$student);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('students',$student);
        $this->assertDatabaseCount('students',$studentBeforeCreate+1);
        $response->assertRedirect(route('students.index'));
    }

    /** @test  */
    public function authenticated_user_can_view_form_create(){
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->CreateRouteViewTest());
        $response->assertViewIs('students.create');
    }

    /** @test  */
    public function unauthenticated_user_can_not_create_student(){
        $student = Student::factory()->make()->toArray();
        $response = $this->post($this->CreateRouteTest(),$student);
        $response->assertRedirect('/login');
    }

    /** @test  */
    public function authenticated_user_can_not_new_student_if_name_field_is_null(){
        $this->actingAs(User::factory()->create());
        $student = Student::factory()->make(['name'=>null])->toArray();
        $response = $this->post($this->CreateRouteTest(),$student);
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function authenticated_user_can_see_name_required_text_if_validate_error()
    {
        $this->actingAs(User::factory()->create());
        $task = Student::factory()->make(['name' => null])->toArray();
        $response = $this->from($this->CreateRouteViewTest())->post($this->CreateRouteTest(),$task);
        $response->assertRedirect($this->CreateRouteViewTest());
    }

    public function CreateRouteTest(){
        return route('students.store');
    }
    public function CreateRouteViewTest(){
        return route('students.create');
    }
}
