<?php

namespace Tests\Feature;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetListStudentTest extends TestCase
{
    /** @test */
    public function user_can_get_all_student(){
        $student = Student::factory()->create();
        $response = $this->get(route('students.index'));
        $response->assertStatus(200);
        $response->assertViewIs('students.index');
        $response->assertSee($student->name);
    }
}
